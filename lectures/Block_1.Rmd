---
title: "R Dive-in workshop"
output:
  pdf_document:
    toc: true
    toc_depth: 3
header-includes:
- \usepackage{fancyhdr}
- \usepackage{graphicx}
- \usepackage{lipsum}
- \pagestyle{fancy}
- \setlength\headheight{80.0pt}
- \addtolength{\textheight}{-80.0pt}
- \fancyhf{}
#- \rhead{UTS -- C3}
#- \lhead{R Dive-in Course }
- \rfoot{\thepage}
- \chead{\includegraphics[width=\textwidth]{img/c3.png}}
#bibliography: skeleton.bib
#link-citations: yes
---

```{r setup, include=FALSE}

library(pander)

# invalidate cache when the tufte version changes
knitr::opts_chunk$set(tidy = FALSE, cache.extra = packageVersion('tufte'))
options(htmltools.dir.version = FALSE)
knitr::opts_knit$set(root.dir = '../input/')
```

\newpage

# Introduction to R
## What is R and why to use it?
- *Free.* As an open-source project, you can use R free of charge, without worrying about subscription fees, license managers, or user limits. 

- *Open:* You can inspect the code and tinker with it as much as you like. 

- *language.* In R, you do data analysis by writing functions and scripts, not by pointing and clicking. That may sound daunting, but it's an easy language to learn, and a very natural and expressive one for data analysis. But once you learn the language, there are many benefits. A script documents all your work, from data access to reporting, and can instantly be re-run at any time. Scripts also make it easy to automate a sequence of tasks that can be integrated into other processes. Many R users who have used other software report that they can do their data analyses in a fraction of time.

- *graphics and data visualization.* It has excellent tools for creating graphics, from staples like bar charts and scatter plots to multi-panel Lattice charts to brand new graphics of your own devising.

- *flexible statistical analysis toolkit*. All of the standard data analysis tools are built right into the R language: from accessing data in various formats, to data manipulation (transforms, merges, aggregations, etc.), to traditional and modern statistical models (regression, ANOVA, GLM, tree models, etc). All are included in an object-oriented framework that makes it easy to programatically extract out and combine just the information you need from the results, rather than having to cut-and-paste from a static report.

- *Access to powerful, cutting-edge analytics*. Leading academics and researches from around the world use R to develop the latest methods in statistics, machine learning, and predictive modeling. There are expansive, cutting-edge edge extensions to R in finance, genomics, and dozens of other fields. To date, more than 2000 packages extending the R language in every domain are available for free download, with more added every day.

- *Unlimited possibilities.* With R, you're not restricted to choosing a pre-defined set of routines. You can use code contributed by others in the open-source community, or extend R with your own functions. And R is excellent for "mash-ups" with other applications: combine R with a MySQL database, an Apache web-server, and the Google Maps API and you've got yourself a real-time GIS analysis toolkit. That's just one big idea -- what's yours?

-*continuous improvements* 

## How does it work?

You can enter commands one at a time at the command prompt (>) in the Console or run a set of commands from a source file (Script). Most functionality is provided through built-in and user-created functions and all data objects are kept in memory during an interactive session. There is a wide variety of data types, including vectors (numerical, character, logical), matrices, data frames and lists. The basic functions are available by default, while other functions can be added from  packages that can be loaded in the current session as needed.

*A key skill to using R effectively is learning how to use the built-in help system `help(foo)`.*

Please, take you time to read some help pages. For example the help for the read.csv() function: `help(read.csv())`


## Instalation

R is multi-platform, and runs well on Linux Windows or Mac-OS. Read the [webpage](https://cran.r-project.org/bin/) to find instructions for you favourite Operative Systems.

I highly recommend to use [RStudio](https://www.rstudio.com/) to interact with R. R Studio is an integrated development environment (IDE) for R. It includes a console, syntax-highlighting editor that supports direct code execution, as well as tools for plotting, history, debugging and workspace management. R Studio is also available in open source editions and also runs on the most common OS's. 

```{r echo=FALSE, out.width='100%'}
knitr::include_graphics('./img/Rstudio.png')
```

*Note: In R Studio, help can be accessed by pressing F1 with the cursor over the function you want help for or in the help panel.*

## Packages

Packages are collections of R **functions** that extend R capabilities. They usually  contain sample data, compiled code and documentation. Currently, the CRAN package repository features more than 9000 packages. The directory where packages are stored is called the library. R comes with some standard packages that are installed when you install R. However, during the course we will need some additional R packages. As these additional packages do not come with the standard installation of R, you will need to install them yourself. Once installed, they have to be loaded into the session to be used `library("name")`.

Packages are split in 'views'. I highly recommend to read the [CRAN Task Views webpage](https://cran.r-project.org/web/views/) in order to find the right packages for your field of interest. 

**Install a package in R**

Once you have successfully installed R on the computer, you can install an additional package by using the function `install.packages("package")` in the R console or using the R Studio tool (*Tools -> Install Packages*).

\newpage
# Data and builtin functions

## Data classes 

R has a wide variety of different **data classes**, including: *numeric, logic, vectors (numerical, character, logical), matrices, data frames and lists*; that you can operate on through **functions**, to  perform statistical analyses, create graphs etc.

It very is important to know and understand the data classes in R, since functions and analysis can be only applied to specific data class. (E.g: You can't sum to characters)

To show (print) the class of the variable, use the `class()` function.

Let's learn the basic data types and some of the build in functions you can apply. I the meantime you will start getting familiar with R studio...

**Numeric variables**

Decimal numbers are called *numerics* in R. It is the default computational data type. If we **assign** a decimal value to a variable `x` as follows, `x` will be of numeric type.

```{r}
x <- 3.14          
```

*Note: Assignemnt can be done in 5 different ways.*

* `x <- value` 

* `x <<- value`

* `value -> x`

* `value ->> x` 

* `x = value`

*Read `help(assignOps)` for more detail. In short: use `x <- value`* 

To retrieve the variable value, you just need to type the variable name in the console. (E.g `x`) 

To check the class of the variable we created in the previous line, we use the `class()` function.
```{r}
class(x)
```

**Functions on numerics**

Typical operations on numeric data are built-in R. The next table summarizes the most common functions you can apply on numeric variables.

```{r, echo=FALSE}
Function=c("abs()","sqrt()","ceiling(x)","floor(x)","trunc(x)",
           "round(x,digits = n)","signif(x,digits = n)",
           "cos(x), sin(x), tan(x)","log)(x)","log10(x)","exp(x)")

Description=c("Absolute Value","Squere Rooot","ceiling(3.475) is 4","floor(3.475) is 3",
              "trunc(5.99) is 5","round(3.475, digits=2) is 3.48",
              "signif(3.475, digits=2) is 3.5" , "also acos(x), cosh(x), acosh(x), etc.",
              "Natural logarithm","Common logarithm","e^x")

t=data.frame(Function,Description)
knitr::kable(t)
```

For example:
```{r}
x <-9 
y <- 4
z <- sqrt(x + y)
z
```


**Logical variables:**

A logical value or boolean can be `True` or `FALSE`. Logical variable are often the result of a comparison between variables and are widely used for flow control in scripts and to select observations in a dataframe. 

```{r}
a <- 1.1; b <- 1.11   
l = a < b          
l
```

Again, to check the class of the variable `l`:

```{r}
class(l)           
```

We will learn how to use logical values for flow control and to select observations soon.

**Logical functions:**

Function |	Description
---------|-------------
& | Logical *and*
\| | Logical *or*
! | Logical *negation*

Let's try some examples:

```{r}
u = TRUE; v = FALSE 

```

```{r}
u & v
```


```{r}
u | v
```

```{r}
!u
```

**Character variables**

Character variables are used to store strings. We can create a string object as follows:

```{r}
s <- "I love R!" 
```

**Character functions**

Some basic character functions are summarized in the following table. More functions for string manipulation can be found in the R documentation. See `help("sub")`
```{r, echo=F}
Function=c("as.character()","substr(x, start=n1, stop=n2)","grep(pattern, x)", 
            "sub(pattern, replacement, x)","strsplit(x, split)","strsplit('abc','')",
            "paste(..., sep='')","toupper(x)","tolower(x)")

Description=c("Attempts to coerce its argument to character type",
              "Extract or replace substrings in a character vector.",
              "Search for pattern in x.", "Find pattern in x and replace with replacement text. ",
              "Split the elements of character vector x at split.","Returns 3 element vector 'a','b','c'",
              "Concatenate strings after using sep string to seperate them.","Uppercase","Lowercase")
t=data.frame(Function,Description)
knitr::kable(t)
```



```{r}
s = as.character(2.73) 
s 
```

Some examples:
Two character values can be concatenated with the paste function.

```{r}
f <- "I like"; s ="statistics" 
p <- paste(f, s) 
```

To extract a substring, we apply the `substr` function. Here is an example showing how to extract the substring between the second and ninth positions in a string.

```{r}
substr(p, start=2, stop=9) 
```


**Vectors**

If you want to store a sequence of data elements of the same type, then you need to use vectors. The members in a vector are called components. To create a vector we use the `c()` function.

```{r}
a <- c(1,2,3)                
b <- c("one","two","three")            
c <- c(TRUE,TRUE,TRUE,FALSE,TRUE,FALSE) 
```

*Vector operations:*

Functions on vectors are applied according to the type of their components (i.e If you have a numeric vector you can apply numeric functions).

For example:

- Vector arithmetic:

Arithmetic operations of vectors are performed member-by-member. You will get a vector of the same length as the input.
```{r}
a+2
a+a
a*a
```

- Combining vectors:
You can use the same `c()` function to combine vectors of the same type (or coercible).
```{r}
c(a,a)
c(a,b)
c(a,c)
```

Note that when combining different basic types, they are coerced to match.!

If two vectors are of unequal length, the shorter one will be recycled in order to match the longer vector. For example, the following vectors $u$ and $v$ have different lengths, and their sum is computed by recycling values of the shorter vector $u$.
  
```{r}
a = c(1, 2, 3) 
b = c(1, 2, 3, 4, 5, 6, 7, 8, 9) 
a + b 
```

which is the same as:
```{r}
a = c(1, 2, 3, 1, 2, 3, 1, 2, 3) 
b = c(1, 2, 3, 4, 5, 6, 7, 8, 9) 
a + b 
```

- Retrieve elements:

We retrieve the elements on a vector by giving it's index inside a single square bracket `[]` operator. For example, if we need the third element of `s`:
```{r}
s = c("a", "b", "c", "d", "e") 
s[3] 
```

We can also retrieve a range of elements using the `id1:id2` operator inside the square brackets.

```{r}
s[2:5]
```

It is also usually very handy to retrieve elements according to it's values. For example if we want to remove outliers from an analysis.
```{r}
b
cond <- b>2
b[cond]
```


- Remove values:

We can use negative indices to strip the member whose position has the same absolute value as the negative index. 

For example, the following creates a vector slice with the third member removed.
```{r}
s[-3] 
```

Note: If an index is out-of-range, a missing value will be reported via the symbol NA.
```{r}
 s[10] 
```

- It is possible to assign names to vector members

For example, the following variable `n` is a character string vector with two members.

```{r}
n = c("John", "Doe")
```

We now can name the first member as First, and the second as Last.

```{r}
names(n) = c("First name", "Last name") 
n
```

Now we are able to retrieve values according to the name, not inly the index.
```{r}
n["First name"] 
```


**Matrices**

A matrix is a collection of data elements of the same type arranged in a two-dimensional rectangular layout. The following is an example of a matrix with 2 rows and 4 columns.

```{r}
M = matrix( 
   c(1, 2, 3, 4, 10, 11, 12,13), 
   nrow=2, ncol=4,              
   byrow = TRUE)       
M 
```

- Subseting:

An element at the $m^{th}$ row, $n^{th}$ column of $M$ can be accessed by the expression `M[m, n]`.
  
```{r}
M[2, 3]
```

The entire $m^{th}$ row of $M$ can be extracted as `M[m, ]`.
```{r}
M[2, ]
```


Similarly, the entire $n^{th}$ column of $M$ can be extracted as `M[ ,n]`.
```{r}
 M[ ,3]     
```


We can also extract more than one row or column at a time using a vector with indices.
```{r}
M[,c(1,3)]
```

**Lists**
A list is a generic vector containing objects of **different** classes.

For example, the following variable `x` is a list containing of three vectors `n`, `s`, `b`.

```{r}
n <- c(2, 3, 5) 
s <- c("aa", "bb", "cc", "dd", "ee") 
b <- c(TRUE, FALSE, TRUE, FALSE, FALSE) 
l <- list(n, s, b)   
names(l) <- c('n','s','l')
l
```

- Sub-setting a list:

We retrieve a list slice with the single square bracket `[]` operator. The following is a slice containing the second member of `x`, which as you can see, a copy of `s`.

```{r}
 l[2] 
```

With an index vector, we can retrieve a slice with multiple members. Here a slice containing the second and third members of x.

```{r}
l[c(2, 3)] 
```

We can also retrieve by name using the `$` operator. For example:
```{r}
l$s
```

**Dataframes**

Is by far the most widely used data type in **R**. A data frame is used for storing data tables. It is a list of vectors of *equal length*. Usually if you import data from a csv or xls file, it will be stored as data frame. For example, the following variable `df` is a data frame containing three vectors `n`, `s`, `b`.


```{r}
Sepal.length = c(5.1,4.9,4.7,NA) 
Sepal.width = c(3,3.1,4,5) 
Specie = c("setosa","setosa","virginica","setosa") 
treated = c(T,T,F,T)
df = data.frame(Sepal.length, Sepal.width, Specie,treated)

df
```

This data type allows you to store your data in a rectangular grid. Each row of this grid corresponds to measurements or values of an instance, while each column is a vector containing data for a specific variable, this means that a data frame's rows do not need to contain the same type of values. Each column needs to consist of values of the same type, since they are data vectors.

To retrieve an element (similar to a cell in excel), we would enter its row and column id's in the single square bracket `[]` operator. The two coordinates are separated by a comma. 

Here is the cell value from the first row, second column of the dataframe
```{r}
df[1, 2] 
```

Moreover, we can use the row and column names instead of the numeric coordinates.

```{r}
 df["1","Specie"]
```

To add more rows (instances/observations) to a dataframe we use the `rbind()` function.

```{r}
new.rows = c(4.7,2.1,"setosa",TRUE,4.4,2.2,"virginica",FALSE)
df = rbind(df,new.rows)
```

to add more columns (variables) we an use `cbind()`:

```{r}
Petal.Length <- c(4.2,4.5,3.6,5.6,3)
df <- cbind(df,Petal.Length)
df
```


**Missing data**

In R, missing values are represented by the symbol NA (not available) . Impossible values (e.g., dividing by zero) are represented by the symbol NaN (not a number). 

- Testing for Missing Values

```{r}
v <- c(1,2,3,NA,3,4,5,NA)
is.na(v)
```

- Recoding Values to Missing

We can for example assign NA to all the observations corresponding to the "virginica" species.
```{r}
df[df$Specie=="virginica",] <- NA
df
```


- Excluding Missing Values from Analyses

If we try to apply numeric functions on data with missing values, R will yield missing values. For example

```{r}
x <- c(1,2,NA,3)
mean(x) 
```

na.rm is generic parameter that can be added to a function call in order to exclude de $NA$ values before trying to apply the function. For example, in order to calculate the mean of `x`, we need to add the `na.rm=TRUE` parameter to the `mean()` function.
```{r}
mean(x, na.rm=TRUE) 
```

A handy function while dealing with missing values is `complete.cases()`. This function will return a logical vector indicating which cases are complete.

```{r}
complete.cases(df)
```

> Excercises: 

> - Use complete cases to get only non NA from `x`

> - Use complete cases to get df without missing values `df`

> - List rows of data that have missing values in `df`. Tip: Also see `na.omit()`


## Data Input

Input and output data operation in **R** will occur on the working directory unless you set the full path in every operation, which is not very convenient. 

To find out the current working directory use the `getwd()` command.

If you need to change the path so that it points to the folder where you have stored your data you can do it with `setdw()` function:

- `setwd("/data/Documents/courses/2016-R/R-Course/") # Linux and Mac`

- `setwd("c:/user/data") # Windows` 

**Spreadsheets:** 

Before you start thinking about how to load your spreadsheet files into R, you need to make sure that your data is well formatted (it needs to be consistent!). If you don't do this, you might experience many problems! Here's a list of some best practices to help you to avoid any issues with reading your spreadsheet files into R:

 - The first row of the spreadsheet is usually reserved for the header, while the first column is used to identify the sampling unit;
 - Avoid names, values or fields with blank spaces, otherwise each word may be interpreted as a separate variable, resulting in errors that are related to the number of elements per line in your data set;
 - If you want to concatenate words, do this by inserting a '.' (e.g Homo.sapiens) or mixed cases (e.g HomoSapiens)
 - Short names are preferred over longer names;
 - Try to avoid using names that contain symbols;
 - Delete any comments that you have made in your file to avoid extra columns or NA's to be added to your file; and
 - Make sure that any missing values in your data set are indicated with **NA**.

**From text file**

The function `read.csv()` reads a file in table format and creates a data frame from it, with cases corresponding to lines and variables to fields in the file. For example, the next command will read data with default parameters and store it in `data_csv`

```{r}
data_csv = read.csv("iris.csv")
```

```{r echo=FALSE}
pander(pandoc.table(head(data_csv)))
```

> Exercise: Import `iris2.csv`.


**From Excel**

It is very similar, but we need an extra package to access the `read.xlsx()` function of the `xlsx` package. If the package is not installed in you system, you should use `install.package("xlsx")`.


```{r}
library(xlsx)
data_xls <- read.xlsx("iris.xls", 1, row.names=1)
```


**Data output**

Is very similar to input but using `write.csv()`, `write.xlxs()`.

> Exercise: Read the help of `write.csv()` and `write.xlsx()` and export df as csv and as xlsx file.

\newpage
# Funtions

## Creating you own functions

 A function is a piece of code written to carry out a specified task; it may accept arguments or parameters and it may return one or more values. In programming, we use functions to incorporate sets of instructions that we want to use repeatedly or that, because of their complexity, are better self-contained in a sub program and called when needed.

This is the syntax to define a function in R: 

```{r, eval=F}
FunctionName <- function(args)
{
computation
return(results)
}
```

Lets say we want to convert temperature values from Fahrenheit to Celsius using a function and return the result. Remember: to get Celsius from Fahrenheit you need deduct $32$, then multiply by $5$, then divide by $9$.

```{r}
F2C <- function(F){
C <- (F-32)*5/9
return(C)
}
```

To use the function to transform 365 Fahrenheit to Kelvin:

```{r}
F2C(365)
```

> Excercice: Create a function to return the temperature in Celcius and Kelvin.

# Control Flow

While coding we want to control the flow of our actions. Control flow is simply the order in which the statements are evaluated. That can be done by setting things to happen only if a condition is met. Alternatively, we can also set an action to be evaluated for a particular number of times or over each item of a sequence. 

R has the standard control structures similar to other programming languages, including `for`, `while`, `if`, `ifelse`. You should know that usually it is far more efficient to use built-in functions rather than control structures whenever possible (e.g, transpose matrix by hand vs. `transpose`; tapply vs for loops). I will talk about it later.

## Conditionals

The simple example of an `if` statement is: `if (condition){expression}`. If the condition is TRUE, the statement gets executed. But if it's FALSE, nothing happens.


For example:
```{r}
leos_age=34
# Replace with your age
your_age=34
if (your_age < leos_age) {print("You are younger than Leo!")}
if (your_age > leos_age) {print("You are older than Leo!")}
if (your_age == leos_age) {print("You are as old as Leo!")}
```

If there are only 2 possible outcomes, we should use an if-else statements like this:
```{r}
if (your_age < leos_age) {print("You are younger than Leo!")} else {print("You are not younger than Leo!")}
```

Or we can nest if-else conditions in case we have more than 2 possible outcomes
```{r}
if (your_age < leos_age) {
   print("You are younger than Leo!")
} else if (your_age > leos_age) {
   print("You are older than Leo!")
} else
   print("You are as old (young) as leo!")
```

If you want to apply a conditional on a vector, you can  use the `ifelse()` function. `ifelse` returns a value with the same shape as test which is filled with elements selected from either yes or no depending on whether the element of test is TRUE or FALSE. (i.e Vectorized). Let's try to understand with and example:

Divide by 10 only those elements on `x` that are greater than 10.
```{r}
 x=c(1,20,3,4,50)
 ifelse(x<10,x,x/10)
```

## Loops

If a task must be repeated many times, a loop may come in handy. We only need to specify how many times or upon which conditions those tasks are executed: we assign initial values to a control loop variable, perform the loop and then, once finished, we typically do something with the results.

Lets say you need to perform the same operation many times:

```{r}
y=2*1
print(y)
y=2*2
print(y)
y=2*3
print(y)
y=2*4
print(y)
y=2*5
print(y)

```


We can replace this with a **for** loop. The **for** loop (used as: `for (var in seq){expr}`) can be used to repeat a set of instructions, and it is used when you know in advance the values that the loop variable will have each time it goes through the loop.


```{r}
for (v in seq(1,5,by=1)) {print(2*v)}
```

another option:

```{r}
x = c(1,2,3,4,5)
for (v in x) {print(2*v)}
```


On the other hand, the **while** loop (`while (cond) {expr}`) can be used to repeat a set of instructions until a condition is not TRUE any more and you do not know in advance how many times the instructions will be executed. 

```{r}
x=0
while(x < 5)
 {
    x <- rnorm(1,mean=2,sd=3)
    print(x)
 }
```



## Vectorization:

Vectorization is the operation of converting repeated operations on simple numbers, into single operations on vectors or matrices.
The above loop constructs can be made implicit by using vectorization. 'Implicit', because they do not really disappear; at a lower level, the alternative vectorized form translates into code which will contain one or more loops in the lower level language the form was implemented and compiled (Fortran, C, or C++ ). These hidden loops are usually faster than the equivalent explicit R code.
For example,
```{r, eval=F}
x = c(1,2,3,4,5)
for (v in x) {print(2*v)}
```
can be translated simply as:
```{r}
print(2*x)
```

If the input is a a matrix or dataframe we should use de `apply()` function.  It will apply a function (built-in or user defined) to the rows (margin=1) or columns (margin=2) of the given matrix or dataframe.

For example:
Define matrix mymat by replicating the sequence 1:5 for 4 times and transforming into a matrix:

```{r}
mymat<-matrix(rep(seq(5), 4), ncol = 5) 
```

If we want to sum by rows using a for loop:
```{r}
rows=seq(1,dim(mymat)[1])
for (r in rows) {print(sum(mymat[r,]))} 
```

Using apply, is much easy (and faster)!

Sum by rows 
```{r}
apply(mymat, 1, sum) 
```


Sum by cols
```{r}
apply(mymat, 2, sum) 
```

In case you don't believe me we will try to  compare the time it takes to run a matrix transposition using nested `for` loops and the `t()` function that is a built in vectorized function. Use `system.time()` to see how long it takes.

```{r, eval=F,echo=F}
my_transpose <- function(x) {
  y <- matrix(0, nrow=ncol(x), ncol=nrow(x))
  for (i in 1:nrow(x)) {
    for (j in 1:ncol(x)) {
      y[j,i] <- x[i,j]
    }
  }
return(y)
}
z <- matrix(1, nrow=5000, ncol=10000)
system.time(my_transpose(z))
system.time(t(z))

```